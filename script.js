// baitap1
function nguyenDuongNhoNhat() {
    var S = 0;
    var n = 0;
    while (S < 10000) {

        S = S + n;
        n++;
    }
    document.getElementById('timSo').innerHTML = n;

}
// baitap2
function tinhTong() {
    var n = document.getElementById('soN').value;
    var x = document.getElementById('soX').value;
    var i = 1;
    var T = 1;
    var S = 0;
    while (i <= n) {
        T = T * x;
        S = S + T;
        i++;

    }
    document.getElementById('ketQua').innerHTML = S;
}
// baitap3
function tinhGiaThua() {
    var nhapN = Number(document.getElementById('nhapN').value);
    var giaiThua = 1;
    var giaTri = 1;
    while (giaTri <= nhapN) {
        giaiThua = giaiThua * giaTri;
        giaTri++;
    }
    document.getElementById('giaiThua').innerHTML = giaiThua;
}
// baitap4
function taoDiv() {
    var divs = document.getElementsByClassName('hihi');
    for (var i = 0; i < divs.length; i++) {
        // Vị trí chẵn => màu đỏ
        if ((i + 1) % 2 == 0) {
            divs[i].style.background = "red";
        } else { // Vị trí lẽ => màu xanh
            divs[i].style.background = "blue";
        }
    }
}